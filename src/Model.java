public class Model {
	private double[][] dots;
	private double[][] segments;
	private double[][] faces;

	public Model() {
		super();
	}
	public Model(int dots, int segments, int faces) {
		super();
		this.dots = new double[dots][3];
		this.segments = new double[segments][2];
		this.faces = new double[faces][3];
	}

	public double[][] getDots() {
		return dots;
	}

	public void setDots(double[][] dots) {
		this.dots = dots;
	}

	public double[][] getSegments() {
		return segments;
	}

	public void setSegments(double[][] segments) {
		this.segments = segments;
	}

	public double[][] getFaces() {
		return faces;
	}

	public void setFaces(double[][] faces) {
		this.faces = faces;
	}

}
