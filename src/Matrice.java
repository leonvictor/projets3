public class Matrice {
	private int rows;
	private int columns;
	private double[][] data;


	public Matrice(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		this.data = new double[rows][columns];
	}

	public Matrice(int rows, int columns, double[][] data) {
		this.rows = rows;
		this.columns = columns;
		this.data = data;
	}

	public Matrice plus(Matrice B) {
		if (B.rows != this.rows || B.columns != this.columns);
		Matrice C = new Matrice(rows, columns);
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				C.data[i][j] = this.data[i][j] + B.data[i][j];
		return C;
	}

	public boolean equals(Matrice B) {
		if (B.rows != this.rows || B.columns != this.columns);
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				if (this.data[i][j] != B.data[i][j]) return false;
		return true;
	}

	public Matrice multiply(Matrice B) {

		if (this.columns != B.rows);
		Matrice C = new Matrice(this.rows, B.columns);
		for (int i = 0; i < C.rows; i++)
			for (int j = 0; j < C.columns; j++)
				for (int k = 0; k < this.columns; k++)
					C.data[i][j] += (this.data[i][k] * B.data[k][j]);
		return C;
	}
	public String toString(){
		String s = "";
		for(int i=0;i<this.rows;i++){
			for(int j=0;j<this.columns;j++){
				s+=this.data[i][j]+" ";
			}
			s+="\n";
		}
		return s;
	}
	//retourne une matrice identique avec une ligne en plus, contenant les données rowData
	public Matrice addRow(double[] rowData){
		double[][] data = new double[rows+1][columns];
		for(int i=0;i<this.rows;i++){
			for(int j=0;j<this.columns;j++){
				System.out.println(i + " " +j);
				data[i][j]=this.data[i][j];
			}
		}
		for(int k=0;k<this.columns;k++){
			data[this.rows][k]=rowData[k];
		}

		return new Matrice(this.rows+1,this.columns,data);
	}
	public Matrice addColumn(double[] columnData){
		double[][] data = new double[rows][columns+1];
		for(int i=0;i<this.rows;i++){
			for(int j=0;j<this.columns;j++){
				data[i][j]=this.data[i][j];
			}
		}
		for(int i=0;i<this.rows;i++){
			data[i][this.columns]=columnData[i];
		}

		return new Matrice(this.rows,this.columns+1,data);
	}
	//retourne une matrice avec la dernière colonne en moins
	public Matrice removeColumn(){
		double[][] data = new double[rows][columns-1];
		for(int i=0;i<this.rows;i++){
			for(int j=0;j<this.columns-1;j++){
				data[i][j]=this.data[i][j];
			}
		}
		return new Matrice(this.rows,this.columns-1,data);

	}

	public Matrice removeRow(){
		double[][] data = new double[rows-1][columns];
		for(int i=0;i<this.rows-1;i++){
			for(int j=0;j<this.columns;j++){
				data[i][j]=this.data[i][j];
			}
		}  	
		return new Matrice(this.rows-1,this.columns,data);

	}
	//(ca devrait marcher) retourne la matrice après application d'une rotation de degré d
	public Matrice rotationX(double d){
		Matrice tmp = this.addRow(new double[]{0,0,0}).addColumn(new double[]{0,0,0,1});
		double[][] data = new double[][]{
				{1,0,0,0},
				{0,Math.cos(d),-(Math.sin(d)),0},
				{0,Math.sin(d),Math.cos(d),0},
				{0,0,0,1}}; 

		tmp = tmp.multiply(new Matrice(4,4,data));
		return tmp.removeRow().removeColumn();
	}

	public Matrice rotationY(double d){
		Matrice tmp = this.addRow(new double[]{0,0,0}).addColumn(new double[]{0,0,0,1});
		double[][] data = new double[][]{
				{Math.cos(d),0,Math.sin(d),0},
				{0,1,0,0},
				{-(Math.sin(d)),0,Math.cos(d),0},
				{0,0,0,1}}; 

		tmp = tmp.multiply(new Matrice(4,4,data));
		return tmp.removeRow().removeColumn();
	}

	public Matrice rotationZ(double d){
		Matrice tmp = this.addRow(new double[]{0,0,0}).addColumn(new double[]{0,0,0,1});
		double[][] data = new double[][]{
				{Math.cos(d),-(Math.sin(d)),0,0},
				{Math.sin(d),Math.cos(d),0,0},
				{0,0,0,1},
				{0,0,0,1}}; 

		tmp = tmp.multiply(new Matrice(4,4,data));
		return tmp.removeRow().removeColumn();
	}

	public Matrice homothetie(double k){
		Matrice tmp = this.addRow(new double[]{0,0,0}).addColumn(new double[]{0,0,0,1});
		double[][] data = new double[][]{
				{k,0,0,0},
				{0,k,0,0},
				{0,0,k,0},
				{0,0,0,1}}; 

		tmp = tmp.multiply(new Matrice(4,4,data));
		return tmp.removeRow().removeColumn();
	}

	//effectue la translation selon le vecteur u(x,y,z)
	public Matrice translation(double x,double y,double z/*ero7*/){
		Matrice tmp = this.addRow(new double[]{0,0,0}).addColumn(new double[]{0,0,0,1});
		double[][] data = new double[][]{
				{1,0,0,x},
				{0,1,0,y},
				{0,0,1,z},
				{0,0,0,1}}; 

		tmp = tmp.multiply(new Matrice(4,4,data));
		return tmp.removeRow().removeColumn();

	}

}
